import React from 'react';
import Favorites from './pages/Favorites';
import Basket from './pages/Basket';
import Home from './pages/Home';
import { useList } from './customHook/useList'
import './style/style.scss'

import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink
} from "react-router-dom";


function App() {

  const [list, setList] = useList();



  // useEffect(() => {
  //   localStorage.setItem('Favorites', JSON.stringify(fav))
  // }, [fav])

  // const addToFav = (card) => {
  //   const newCard = card;
  //   newCard.active = false;
  //   newCard.inFavorite = true;
  //   setFav(favIds => [...favIds, newCard]);
  // }

  // const removeFav = (card) => {
  //   const allFav = localStorage.getItem('Favorites');
  //   const newCard = card;
  //   newCard.inBasket = false;
  //   const allFavFilters = JSON.parse(allFav);
  //   setFav(allFavFilters.filter(item => item.id !== card.id));
  // }

  // useEffect(() => { localStorage.setItem('Basket', JSON.stringify(basket)) },
  //   [basket])

  // const addToBasket = (card) => {
  //   const newCard = card;
  //   newCard.inBasket = true;
  //   setBasket(idsInBasket => [...idsInBasket, card]);
  // }


  //ANOTHER VERSION
  const addToFav = (card) => {

    const allFavItems = localStorage.getItem('Favorites') ? JSON.parse(localStorage.getItem('Favorites')) : []

    const newCard = card;
    newCard.inFavorite = true;
    allFavItems.push(newCard);
    localStorage.setItem('Favorites', JSON.stringify(allFavItems));

    setList(list.map(item => {
      if (item.id === card.id) {
        item.inFavorite = true;
      }
      return item;
    }))
  }

  const removeFav = (card) => {

    const allFavItems = JSON.parse(localStorage.getItem('Favorites'));
    const arr = allFavItems.filter(item => item.id !== card.id);
    localStorage.setItem('Favorites', JSON.stringify(arr));

    setList(list.map(item => {
      if (item.id === card.id) {
        item.inFavorite = false;
      }
      return item;
    }))
  }

  const addToBasket = (card) => {

    const allBasketItems = localStorage.getItem('Basket') ? JSON.parse(localStorage.getItem('Basket')) : []

    const newCard = card;
    newCard.inBasket = true;
    allBasketItems.push(newCard);
    localStorage.setItem('Basket', JSON.stringify(allBasketItems));

    setList(list.map(item => {
      if (item.id === card.id) {
        item.inBasket = true;
      }
      return item;
    }))
  }

  const removeBasket = (card) => {

    const allBasketItems = JSON.parse(localStorage.getItem('Basket'));
    const arr = allBasketItems.filter(item => item.id !== card.id);
    localStorage.setItem('Basket', JSON.stringify(arr));

    setList(list.map(item => {
      if (item.id === card.id) {
        item.inBasket = false;
      }
      return item;
    }))
  }

  return (
    <Router>
      <div className={'app'}>

        <header className="header">
          <NavLink to="/" exact>Home</NavLink>
          <NavLink to="/fav">Favorites</NavLink>
          <NavLink to="/basket">Basket</NavLink>
        </header>

        <Switch>
          <Route path="/fav">
            <Favorites
              items={list}
              addToFav={addToFav}
              addToBasket={addToBasket}
              removeFav={removeFav}
            />
          </Route>

          <Route path="/basket">
            <Basket
              page='basket'
              items={list}
              addToFav={addToFav}
              addToBasket={addToBasket}
              removeFav={removeFav}
              removeBasket={removeBasket}
            />
          </Route>

        <Route path="/">
            <Home
              items={list}
              addToFav={addToFav}
              addToBasket={addToBasket}
              removeFav={removeFav}
            />
          </Route>

        </Switch>
      </div>
    </Router>
  );
}


export default App;




// const allFacvItems = localStorage.getItem('Favorites') ? JSON.parse(localStorage.getItem('Favorites')) : []
// for(let item in list){
//   console.log(item);
//   for(let fav in allFacvItems){
//     if(item.id == fav.id){
//       item.inFavorite = true;
//     }
//     console.log(fav);
//   }
// }
// setList(list);