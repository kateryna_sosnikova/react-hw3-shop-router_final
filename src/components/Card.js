import React from 'react';
import { useState } from 'react';
import PropTypes from 'prop-types';

import Button from './Button'
import Modal from './Modal'
import { FaRegStar } from "react-icons/fa";
import { FaStar } from "react-icons/fa";
import { FaTrash } from "react-icons/fa";


const Card = (props) => {

    const { name, price, article, url, color, inFavorite, inBasket, addToFav, removeFav, addToBasket, removeBasket, id, card, page } = props;

    const [modalState, setModalState] = useState(false);
    const [modalDeleteState, setModalDeleteState] = useState(false);

    const btnContent = {
        text: "Add To Card",
        onClick: () => setModalState(true)
    }

    const modalContent = {
        text: "Вы уверены, что хотите добавить товар в корзину?",
        header: "Товар будет добавлен в корзину",
        onClick: () => setModalState(false),
        actions: [<Button text="ОК" onClick={() => { addToBasket(card); setModalState(false) }} />,
        <Button text="Нет, не нужно" onClick={() => setModalState(false)} />]
    }

    const modalRemoveContent = {
        text: "Вы уверены, что хотите удалить товар?",
        header: "Товар будет безвозвратно удален и вы можете забыть о нем",
        onClick: () => setModalDeleteState(false),
        actions: [<Button text="ОК" onClick={() => { removeBasket(card); setModalDeleteState(false) }} />,
        <Button text="Нет, хочу оставить" onClick={() => setModalDeleteState(false)} />]
    }


    return (
        <li className="card-item" id={id}>
            <img className="card-image" src={url} alt='item-pic' />
            { !inFavorite &&
                <span className="favorite-icon" onClick={() => { addToFav(card); }}><FaRegStar /></span>}
            { inFavorite && <span className="favorite-icon" onClick={() => { removeFav(card); }}><FaStar /></span>}
            { inBasket && page && <span className="trash-icon" onClick={() => {setModalDeleteState(true)}}><FaTrash /></span>}
            { modalDeleteState && 
                <Modal {...modalRemoveContent} />}
            <h2 className="card-name">{name}</h2>
            <p className="card-article">Article: {article}</p>
            <p className="card-color">Color: {color}</p>
            <p className="card-price">Price: {price}</p>
            {!page && <Button {...btnContent} />}
            { modalState &&
                <Modal {...modalContent} />}
        </li>

    )
}

export default Card;

Card.propTypes = {
    name: PropTypes.string,
    price: PropTypes.string,
    article: PropTypes.string,
    url: PropTypes.string,
    color: PropTypes.string,
    id: PropTypes.number
}

Card.defaultProps = {
    price: '$9.99'
}