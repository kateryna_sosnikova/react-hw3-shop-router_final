import { useEffect, useState } from 'react';

export const useList = () => {

    const [list, setList] = useState([]);

    useEffect(() => {

        const arrFav = localStorage.getItem('Favorites') ? JSON.parse(localStorage.getItem('Favorites')) : []
        const arrayBasket = localStorage.getItem('Basket') ? JSON.parse(localStorage.getItem('Basket')) : []

        if (arrFav || arrayBasket) {

            fetch('./product.json')
                .then(res => res.json())
                .then(data => {
                    setList(data.map(item => {
                        if (arrFav) {
                            arrFav.forEach(card => {
                                if (item.id === card.id) {
                                    item.inFavorite = true;
                                }
                            })
                        }
                        if (arrayBasket) {
                            arrayBasket.forEach(card => {
                                if (item.id === card.id) {
                                    item.inBasket = true;
                                }
                            })
                        }
                        return item;
                    }))
                })
        } else {
            fetch('./product.json')
                .then(res => res.json())
                .then(data => setList(data))
        }

        // if (JSON.parse(localStorage.getItem('Favorites')) === null) {
        //   setFav([])
        // } else {
        //   setFav(JSON.parse(localStorage.getItem('Favorites')))
        // }

        // if (JSON.parse(localStorage.getItem('Basket')) === null) {
        //   setBasket([])
        // } else {
        //   setBasket(JSON.parse(localStorage.getItem('Basket')))
        // }

    }, [])

    return [list, setList]
}