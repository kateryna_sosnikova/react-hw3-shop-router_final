import React from 'react';
import Card from '../components/Card'

const Basket = (props) => {

    const { items, addToFav, removeFav, addToBasket, removeBasket, page } = props;

    return (
        <>
            <ul className='cards-container'>
                {
                    items.map(item => {
                        if (item.inBasket) {
                            return (
                                <Card
                                    {...item}
                                    key={item.id}
                                    addToFav={addToFav}
                                    removeFav={removeFav}
                                    addToBasket={addToBasket}
                                    removeBasket={removeBasket}
                                    card={item}
                                    page={page}
                                />
                            )
                        };
                    }
                    )}
            </ul>
        </>
    )

}

export default Basket;