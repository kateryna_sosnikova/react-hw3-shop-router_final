import React from 'react';
import Card from '../components/Card'

const Favorites = (props) => {

    const { items, addToFav, removeFav, addToBasket } = props;
   
    return (
        <>
            <ul className='cards-container'>
                {
                    items.map(item => {
                        if (item.inFavorite) {
                            return (<Card
                                {...item}
                                key={item.id}
                                addToFav={addToFav}
                                removeFav={removeFav}
                                addToBasket={addToBasket}
                                card={item}
                            />)
                        }
                    }
                    )}
            </ul>
        </>
    )
}

export default Favorites;



