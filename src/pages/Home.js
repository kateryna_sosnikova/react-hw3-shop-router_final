import React from 'react';
import Card from '../components/Card';

function Home(props) {

    const { items, addToFav, removeFav, addToBasket } = props;
    
    return (
        <>
            <ul className='cards-container'>
                {
                    items.map(item =>
                        <Card
                            {...item}
                            key={item.id}
                            addToFav={addToFav}
                            removeFav={removeFav}
                            addToBasket={addToBasket}
                            card={item}
                        />
                    )}
            </ul>
        </>
    )
}

export default Home;